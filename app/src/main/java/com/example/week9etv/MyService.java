package com.example.week9etv;

//Student Name - Jake O'Callaghan
//Student ID - 21339724

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;


public class MyService extends Service {

    private MediaPlayer mediaPlayer;

    public class LocalBinder extends Binder {
        public MyService getService() {
            return MyService.this;
        }
    }

    private final IBinder binder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create and start the MediaPlayer
        mediaPlayer = MediaPlayer.create(this, R.raw.sampleaudio);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop and release the MediaPlayer
        mediaPlayer.stop();
        mediaPlayer.release();
    }
}

